type pos = int
type svalue = Tokens.svalue
type('a,'b) token = ('a,'b) Tokens.token
type lexresult = (svalue,pos) token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
fun err(p1,p2) = ErrorMsg.error p1

fun str2int(s) = foldl (fn(a,r) => ord(a) - ord(#"0") + 10*r) 0 (explode s)
fun eof() = let val pos = hd(!linePos) in Tokens.EOF(pos,pos) end

%%
%header (functor ExprLexFun(structure Tokens: Expr_TOKENS));
%%
\n => (lineNum := !lineNum + 1; linePos := yypos::(!linePos); continue());

"+" => (Tokens.PLUS(yypos, yypos + 1));
"-" => (Tokens.MINUS(yypos, yypos + 1));
"*" => (Tokens.TIMES(yypos, yypos + 1));
"/" => (Tokens.DIVIDE(yypos, yypos + 1));
">" => (Tokens.GT(yypos, yypos + 1));
"<" => (Tokens.LT(yypos, yypos + 1));
">=" => (Tokens.GTE(yypos, yypos + 1));
"<=" => (Tokens.LTE(yypos, yypos + 1));
"!=" => (Tokens.NE(yypos, yypos + 2));
"==" => (Tokens.EQ(yypos, yypos + 2));
"=" => (Tokens.ASSIGN(yypos, yypos + 1));
"," => (continue());

";" => (Tokens.SEMICOLON(yypos, yypos + 1));
"(" => (Tokens.LPAREN(yypos, yypos + 1));
")" => (Tokens.RPAREN(yypos, yypos + 1));
"{" => (Tokens.LBRACE(yypos, yypos + 1));
"}" => (Tokens.RBRACE(yypos, yypos + 1));
"int" => (Tokens.VAR(yypos, yypos + 3));
"if" => (Tokens.IF(yypos, yypos + 2));
"else" => (Tokens.ELSE(yypos, yypos + 4));
"while" => (Tokens.WHILE(yypos, yypos + 5));
"return" => (Tokens.RETURN(yypos, yypos + 6));

[0-9]+ => (Tokens.INT(str2int yytext, yypos, yypos + size yytext));
[a-zA-Z_]+ => (Tokens.ID(yytext, yypos, yypos + size yytext));

[ \t\b\f\r]+ => (continue());

. => (ErrorMsg.error yypos ("illegal  " ^ yytext);continue());
