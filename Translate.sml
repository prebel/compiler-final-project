structure Translate =
struct
fun Expr (Ast.Const (c)) = Int.toString c
| Expr (Ast.Op (a,Ast.Plus,b)) = let
					val a = Expr a
					val b = Expr b
					val c = String.concat [a,"+"]
					val d = String.concat [c,b]
				in
					d
				end
| Expr (Ast.Op (a,Ast.Minus,b)) = let
					val a = Expr a
					val b = Expr b
					val c = String.concat [a,"-"]
					val d = String.concat [c,b]
				in
					d
				end
| Expr (Ast.Op (a,Ast.Mul,b)) = let
					val a = Expr a
					val b = Expr b
					val c = String.concat [a,"*"]
					val d = String.concat [c,b]
				in
					d
				end
| Expr (Ast.Op (a,Ast.Divi,b)) = let
					val a = Expr a
					val b = Expr b
					val c = String.concat [a,"/"]
					val d = String.concat [c,b]
				in
					d
				end
| Expr (Ast.Identifier (id)) = id
fun Exprs [] = "" 
|Exprs [x] = Expr x
| Exprs (x::xs) = let
			val e = Expr x
			val erest = Exprs xs
			val ret = String.concat [e,",",erest]
		in
			ret
			
		end
fun Cond (Ast.LT (expr1,expr2)) = let
					val expr1 = Expr expr1
					val expr2 = Expr expr2
					val a = String.concat [ expr1, " "]
					val a = String.concat [a, "<"]
					val a = String.concat [a," "]
					val a = String.concat [a,expr2]
				in
					a
				end
| Cond	(Ast.GT(expr1,expr2)) = let
					val expr1 = Expr expr1
					val expr2 = Expr expr2
					val a = String.concat [ expr1, " "]
					val a = String.concat [a, ">"]
					val a = String.concat [a," "]
					val a = String.concat [a,expr2]
				in
					a
				end
| Cond	(Ast.LTE(expr1,expr2)) = let
					val expr1 = Expr expr1
					val expr2 = Expr expr2
					val a = String.concat [ expr1, " "]
					val a = String.concat [a, "<="]
					val a = String.concat [a," "]
					val a = String.concat [a,expr2]
				in
					a
				end
| Cond	(Ast.GTE(expr1,expr2)) = let
					val expr1 = Expr expr1
					val expr2 = Expr expr2
					val a = String.concat [ expr1, " "]
					val a = String.concat [a, ">="]
					val a = String.concat [a," "]
					val a = String.concat [a,expr2]
				in
					a
				end
| Cond	(Ast.EQ(expr1,expr2))= let
					val expr1 = Expr expr1
					val expr2 = Expr expr2
					val a = String.concat [ expr1, " "]
					val a = String.concat [a, "=="]
					val a = String.concat [a," "]
					val a = String.concat [a,expr2]
				in
					a
				end
| Cond	(Ast.NE(expr1,expr2)) = let
					val expr1 = Expr expr1
					val expr2 = Expr expr2
					val a = String.concat [ expr1, " "]
					val a = String.concat [a, "!="]
					val a = String.concat [a," "]
					val a = String.concat [a,expr2]
				in
					a
				end
| Cond	(Ast.EXP(expr)) = let 
				val expr = Expr expr
			in
				expr
			end
fun compileExpr (Ast.Int (id)) = [String.concat ["var ",String.concat[id," ;"]]]
| compileExpr (Ast.Assignment (id,expr)) = let
						val expr = Expr expr
					   in
					   	[String.concat [String.concat[id,"="], String.concat [expr,";"]]]
					   end
| compileExpr (Ast.IfThen (cond,stmt)) = let
						val cond = Cond cond
						val stmt = compileExpr stmt
						val stmt = List.nth (stmt,0)
						val a = String.concat["if(",cond]
						val a = String.concat [a,") {"]
						val a = String.concat [a,stmt]
						val a = String.concat [a,"}"]
					in 
						[a]
					end
| compileExpr (Ast.IfThenElse (cond,stmt1,stmt2)) = let
						val cond = Cond cond
						val stmt1 = compileExpr stmt1
						val stmt1 = List.nth (stmt1,0)
						val stmt2 = compileExpr stmt2
						val stmt2 = List.nth (stmt2,0)
						val a = String.concat["if(",cond]
						val a = String.concat [a,") {"]
						val a = String.concat [a,stmt1]
						val a = String.concat [a,"}else{"]
						val a = String.concat [a,stmt2]
						val a = String.concat [a,"}"];
					in 
						[a]
					end
| compileExpr (Ast.While (cond,stmt)) = let
						val cond = Cond cond
						val stmt = compileExpr stmt
						val stmt = List.nth (stmt,0)
						val a = String.concat["while(",cond]
						val a = String.concat [a,") {"]
						val a = String.concat [a,stmt]
						val a = String.concat [a,"}"]
					in
						[a]
					end	
| compileExpr (Ast.FunStat (var,name,arg)) = 
						let
							val a = String.concat[var,"=",name,"("]
							val e = Exprs arg
							val a = String.concat[a,e,")",";"]
						in
							[a]
						end
							



fun compileStmts []        = []
  | compileStmts (x :: xs) = (compileExpr x)  @ (compileStmts xs)
fun  getArgs [] = ""
|   getArgs [Ast.Dec (id)] = id
|   getArgs (Ast.Dec(id) :: xs) = let
					val a = String.concat [id,","]
					val b = getArgs xs
				in
					String.concat[a,b]
				end
fun compileFuncs [] = []
| compileFuncs (Ast.FUNC (a,b,c,d)::xs) = 
						let
							val a = String.concat ["function ",a]
							val a = String.concat [a," ("]
							val x = getArgs b
							val a = String.concat[a,x]
							val a = String.concat[a, ")"]
							val a = String.concat[a, "{"]
							val x = String.concat (compileStmts c)
							val a = String.concat [a , x,"return ",d,";","}"]
							val x = compileFuncs xs
						in
							(a::x)
						end
							
fun compile funcLst = 
				let 
					
					val ret = String.concat (compileFuncs funcLst)
					
				in
					
					ret
				end
				
fun writeFile filename a  =
	    let
	    val content = compile a
	    val fd = TextIO.openOut filename
	        val _ = TextIO.output (fd, content) handle e => (TextIO.closeOut fd; raise e)
	        val _ = TextIO.closeOut fd
	    in () end
end
