structure Ast =
struct

datatype BinOp = Plus | Minus | Mul | Divi;

datatype Expr = Const of int
		|Identifier of string
		| Op 	of Expr * BinOp * Expr;
datatype Arg = 	Dec of string

datatype Cond = LT of Expr * Expr
		| GT of Expr * Expr
		| LTE of Expr * Expr
		| GTE of Expr * Expr
		| EQ of Expr * Expr
		| NE of Expr * Expr
		| EXP of Expr
		
datatype Stmt = Int of string
		| Assignment 	of string * Expr
		| IfThen 	of Cond * Stmt
		| IfThenElse 	of Cond * Stmt * Stmt
		| While 	of Cond * Stmt 
		| FunStat		of string * string * Expr list
		
		
datatype Function = FUNC of string * Arg list * Stmt list * string
fun plus  a b = Op (a, Plus, b)
fun minus a b = Op (a, Minus, b)
fun mul   a b = Op (a, Mul, b)
fun Div   a b = Op (a, Divi, b)
end

