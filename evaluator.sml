structure Parse  =
struct
structure ExprLrVals = ExprLrValsFun(structure Token = LrParser.Token);
structure ExprLex = ExprLexFun(structure Tokens = ExprLrVals.Tokens);
structure ExprParser = Join(
				structure  ParserData 	= ExprLrVals.ParserData
				structure  Lex 		= ExprLex
				structure  LrParser 	= LrParser);
  fun parse filename =
      let val _ = (ErrorMsg.reset(); ErrorMsg.fileName := filename)
	  val file = TextIO.openIn filename
	  fun get _ = TextIO.input file
	  fun parseerror(s,p1,p2) = ErrorMsg.error p1 s
	  val lexer = LrParser.Stream.streamify (ExprLex.makeLexer get)
	  val (absyn, _) = ExprParser.parse(0,lexer,parseerror,())
       in TextIO.closeIn file;
       absyn
      end handle LrParser.ParseError => raise ErrorMsg.Error

end
