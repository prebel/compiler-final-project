functor ExprLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Expr_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct



end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\030\000\003\000\029\000\000\000\
\\001\000\001\000\030\000\003\000\033\000\000\000\
\\001\000\002\000\019\000\003\000\018\000\016\000\017\000\018\000\016\000\000\000\
\\001\000\003\000\006\000\000\000\
\\001\000\003\000\011\000\000\000\
\\001\000\003\000\025\000\000\000\
\\001\000\003\000\026\000\000\000\
\\001\000\004\000\046\000\005\000\048\000\006\000\045\000\007\000\044\000\
\\008\000\043\000\000\000\
\\001\000\005\000\034\000\000\000\
\\001\000\005\000\035\000\000\000\
\\001\000\005\000\072\000\000\000\
\\001\000\015\000\024\000\000\000\
\\001\000\019\000\050\000\000\000\
\\001\000\019\000\070\000\000\000\
\\001\000\019\000\071\000\000\000\
\\001\000\019\000\076\000\000\000\
\\001\000\020\000\012\000\000\000\
\\001\000\020\000\051\000\000\000\
\\001\000\020\000\062\000\000\000\
\\001\000\020\000\074\000\000\000\
\\001\000\021\000\007\000\000\000\
\\001\000\021\000\022\000\000\000\
\\001\000\021\000\023\000\000\000\
\\001\000\022\000\010\000\000\000\
\\001\000\022\000\036\000\000\000\
\\001\000\022\000\047\000\000\000\
\\001\000\022\000\068\000\000\000\
\\001\000\023\000\020\000\000\000\
\\001\000\024\000\000\000\000\000\
\\001\000\024\000\005\000\000\000\
\\078\000\000\000\
\\079\000\000\000\
\\080\000\002\000\004\000\000\000\
\\081\000\000\000\
\\082\000\002\000\009\000\000\000\
\\083\000\000\000\
\\084\000\002\000\019\000\003\000\018\000\016\000\017\000\018\000\016\000\000\000\
\\085\000\000\000\
\\086\000\000\000\
\\087\000\017\000\073\000\000\000\
\\088\000\000\000\
\\089\000\000\000\
\\090\000\000\000\
\\091\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\000\000\
\\092\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\000\000\
\\093\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\000\000\
\\094\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\000\000\
\\095\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\000\000\
\\096\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\000\000\
\\097\000\004\000\046\000\006\000\045\000\007\000\044\000\008\000\043\000\
\\009\000\042\000\010\000\041\000\011\000\040\000\012\000\039\000\
\\013\000\038\000\014\000\037\000\000\000\
\\098\000\000\000\
\\099\000\001\000\030\000\003\000\029\000\000\000\
\\099\000\001\000\030\000\003\000\029\000\004\000\046\000\006\000\045\000\
\\007\000\044\000\008\000\043\000\000\000\
\\100\000\000\000\
\\101\000\000\000\
\\101\000\021\000\049\000\000\000\
\\102\000\007\000\044\000\008\000\043\000\000\000\
\\103\000\007\000\044\000\008\000\043\000\000\000\
\\104\000\000\000\
\\105\000\000\000\
\"
val actionRowNumbers =
"\032\000\029\000\003\000\030\000\
\\020\000\034\000\023\000\004\000\
\\016\000\034\000\036\000\033\000\
\\027\000\036\000\021\000\022\000\
\\011\000\005\000\006\000\035\000\
\\000\000\000\000\001\000\008\000\
\\009\000\024\000\049\000\054\000\
\\053\000\025\000\007\000\055\000\
\\037\000\012\000\017\000\000\000\
\\000\000\000\000\000\000\000\000\
\\000\000\000\000\000\000\000\000\
\\000\000\018\000\038\000\051\000\
\\032\000\002\000\048\000\047\000\
\\045\000\046\000\043\000\044\000\
\\059\000\058\000\057\000\056\000\
\\002\000\026\000\052\000\031\000\
\\013\000\014\000\010\000\050\000\
\\041\000\039\000\042\000\019\000\
\\002\000\015\000\040\000\028\000"
val gotoT =
"\
\\007\000\001\000\008\000\075\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\006\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\006\000\011\000\000\000\
\\004\000\013\000\005\000\012\000\000\000\
\\000\000\
\\000\000\
\\004\000\013\000\005\000\019\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\026\000\003\000\025\000\000\000\
\\001\000\026\000\003\000\029\000\000\000\
\\001\000\030\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\050\000\000\000\
\\001\000\051\000\000\000\
\\001\000\052\000\000\000\
\\001\000\053\000\000\000\
\\001\000\054\000\000\000\
\\001\000\055\000\000\000\
\\001\000\056\000\000\000\
\\001\000\057\000\000\000\
\\001\000\058\000\000\000\
\\001\000\059\000\000\000\
\\000\000\
\\000\000\
\\001\000\062\000\002\000\061\000\000\000\
\\007\000\063\000\000\000\
\\004\000\064\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\065\000\000\000\
\\000\000\
\\001\000\062\000\002\000\067\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\073\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 76
val numrules = 28
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | ID of  (string)
 | INT of  (int) | PROGRAM of  (Ast.Function list)
 | FUNCTIONS of  (Ast.Function list) | ARGS of  (Ast.Arg list)
 | STMTS of  (Ast.Stmt list) | STMT of  (Ast.Stmt)
 | COND of  (Ast.Cond) | EXPRS of  (Ast.Expr list)
 | EXPR of  (Ast.Expr)
end
type svalue = MlyValue.svalue
type result = Ast.Function list
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
nil
val noShift = 
fn (T 23) => true | _ => false
val showTerminal =
fn (T 0) => "INT"
  | (T 1) => "VAR"
  | (T 2) => "ID"
  | (T 3) => "PLUS"
  | (T 4) => "SEMICOLON"
  | (T 5) => "MINUS"
  | (T 6) => "TIMES"
  | (T 7) => "DIVIDE"
  | (T 8) => "GT"
  | (T 9) => "LT"
  | (T 10) => "GTE"
  | (T 11) => "LTE"
  | (T 12) => "EQ"
  | (T 13) => "NE"
  | (T 14) => "ASSIGN"
  | (T 15) => "IF"
  | (T 16) => "ELSE"
  | (T 17) => "WHILE"
  | (T 18) => "RBRACE"
  | (T 19) => "LBRACE"
  | (T 20) => "LPAREN"
  | (T 21) => "RPAREN"
  | (T 22) => "RETURN"
  | (T 23) => "EOF"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19) $$ (T 18) $$ (T 17)
 $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12) $$ (T 11) $$ (T 10)
 $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3) $$ (T 
1)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( _, _, EOF1right)) :: ( _, ( MlyValue.FUNCTIONS 
FUNCTIONS, FUNCTIONS1left, _)) :: rest671)) => let val  result = 
MlyValue.PROGRAM ( FUNCTIONS)
 in ( LrTable.NT 7, ( result, FUNCTIONS1left, EOF1right), rest671)
end
|  ( 1, ( ( _, ( MlyValue.FUNCTIONS FUNCTIONS, _, FUNCTIONS1right)) ::
 _ :: _ :: ( _, ( MlyValue.ID ID2, _, _)) :: _ :: ( _, ( 
MlyValue.STMTS STMTS, _, _)) :: _ :: _ :: ( _, ( MlyValue.ARGS ARGS, _
, _)) :: _ :: ( _, ( MlyValue.ID ID1, _, _)) :: ( _, ( _, VAR1left, _)
) :: rest671)) => let val  result = MlyValue.FUNCTIONS (
Ast.FUNC (ID1,ARGS,STMTS,ID2)::FUNCTIONS)
 in ( LrTable.NT 6, ( result, VAR1left, FUNCTIONS1right), rest671)
end
|  ( 2, ( rest671)) => let val  result = MlyValue.FUNCTIONS ([])
 in ( LrTable.NT 6, ( result, defaultPos, defaultPos), rest671)
end
|  ( 3, ( ( _, ( MlyValue.ARGS ARGS, _, ARGS1right)) :: ( _, ( 
MlyValue.ID ID, _, _)) :: ( _, ( _, VAR1left, _)) :: rest671)) => let
 val  result = MlyValue.ARGS (Ast.Dec(ID) :: ARGS)
 in ( LrTable.NT 5, ( result, VAR1left, ARGS1right), rest671)
end
|  ( 4, ( rest671)) => let val  result = MlyValue.ARGS ([])
 in ( LrTable.NT 5, ( result, defaultPos, defaultPos), rest671)
end
|  ( 5, ( ( _, ( MlyValue.STMTS STMTS, _, STMTS1right)) :: ( _, ( 
MlyValue.STMT STMT, STMT1left, _)) :: rest671)) => let val  result = 
MlyValue.STMTS ( STMT :: STMTS)
 in ( LrTable.NT 4, ( result, STMT1left, STMTS1right), rest671)
end
|  ( 6, ( rest671)) => let val  result = MlyValue.STMTS ([])
 in ( LrTable.NT 4, ( result, defaultPos, defaultPos), rest671)
end
|  ( 7, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.ID ID, _,
 _)) :: ( _, ( _, VAR1left, _)) :: rest671)) => let val  result = 
MlyValue.STMT ( Ast.Int(ID)     )
 in ( LrTable.NT 3, ( result, VAR1left, SEMICOLON1right), rest671)
end
|  ( 8, ( ( _, ( _, _, SEMICOLON1right)) :: ( _, ( MlyValue.EXPR EXPR,
 _, _)) :: _ :: ( _, ( MlyValue.ID ID, ID1left, _)) :: rest671)) =>
 let val  result = MlyValue.STMT (Ast.Assignment (ID,EXPR))
 in ( LrTable.NT 3, ( result, ID1left, SEMICOLON1right), rest671)
end
|  ( 9, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.STMT STMT, _,
 _)) :: _ :: _ :: ( _, ( MlyValue.COND COND, _, _)) :: _ :: ( _, ( _, 
IF1left, _)) :: rest671)) => let val  result = MlyValue.STMT (
 Ast.IfThen  (COND,STMT) )
 in ( LrTable.NT 3, ( result, IF1left, RBRACE1right), rest671)
end
|  ( 10, ( ( _, ( _, _, RBRACE2right)) :: ( _, ( MlyValue.STMT STMT2,
 _, _)) :: _ :: _ :: _ :: ( _, ( MlyValue.STMT STMT1, _, _)) :: _ :: _
 :: ( _, ( MlyValue.COND COND, _, _)) :: _ :: ( _, ( _, IF1left, _))
 :: rest671)) => let val  result = MlyValue.STMT (
 Ast.IfThenElse  (COND,STMT1,STMT2) )
 in ( LrTable.NT 3, ( result, IF1left, RBRACE2right), rest671)
end
|  ( 11, ( ( _, ( _, _, RBRACE1right)) :: ( _, ( MlyValue.STMT STMT, _
, _)) :: _ :: _ :: ( _, ( MlyValue.COND COND, _, _)) :: _ :: ( _, ( _,
 WHILE1left, _)) :: rest671)) => let val  result = MlyValue.STMT (
 Ast.While (COND,STMT))
 in ( LrTable.NT 3, ( result, WHILE1left, RBRACE1right), rest671)
end
|  ( 12, ( ( _, ( _, _, SEMICOLON1right)) :: _ :: ( _, ( 
MlyValue.EXPRS EXPRS, _, _)) :: _ :: ( _, ( MlyValue.ID ID2, _, _)) ::
 _ :: ( _, ( MlyValue.ID ID1, ID1left, _)) :: rest671)) => let val  
result = MlyValue.STMT (Ast.FunStat (ID1,ID2,EXPRS))
 in ( LrTable.NT 3, ( result, ID1left, SEMICOLON1right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.COND (Ast.LT (EXPR1,EXPR2))
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 14, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.COND (Ast.GT(EXPR1,EXPR2))
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 15, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.COND (Ast.LTE(EXPR1,EXPR2))
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 16, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.COND (Ast.GTE(EXPR1,EXPR2))
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 17, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.COND (Ast.EQ(EXPR1,EXPR2))
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.COND (Ast.NE(EXPR1,EXPR2))
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.EXPR EXPR, EXPR1left, EXPR1right)) :: 
rest671)) => let val  result = MlyValue.COND (Ast.EXP EXPR)
 in ( LrTable.NT 2, ( result, EXPR1left, EXPR1right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.EXPRS EXPRS, _, EXPRS1right)) :: ( _, ( 
MlyValue.EXPR EXPR, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.EXPRS (EXPR :: EXPRS)
 in ( LrTable.NT 1, ( result, EXPR1left, EXPRS1right), rest671)
end
|  ( 21, ( rest671)) => let val  result = MlyValue.EXPRS ([])
 in ( LrTable.NT 1, ( result, defaultPos, defaultPos), rest671)
end
|  ( 22, ( ( _, ( MlyValue.INT INT, INT1left, INT1right)) :: rest671))
 => let val  result = MlyValue.EXPR (Ast.Const INT)
 in ( LrTable.NT 0, ( result, INT1left, INT1right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.ID ID, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.EXPR (Ast.Identifier ID)
 in ( LrTable.NT 0, ( result, ID1left, ID1right), rest671)
end
|  ( 24, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.EXPR (Ast.plus EXPR1 EXPR2)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 25, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.EXPR (Ast.minus EXPR1 EXPR2)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 26, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.EXPR (Ast.mul EXPR1 EXPR2)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 27, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( 
MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = 
MlyValue.EXPR (Ast.Div EXPR1 EXPR2)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.PROGRAM x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Expr_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun INT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.INT i,p1,p2))
fun VAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.ID i,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMICOLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun DIVIDE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun GT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun LT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun GTE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun LTE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun NE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun ASSIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun WHILE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun RBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun LBRACE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun RETURN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
end
end
